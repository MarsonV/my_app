defmodule MyAppWeb.FallbackController do
    @moduledoc """
    Translates controller action results into valid `Plug.Conn` responses.

    See `Phoenix.Controller.action_fallback/1` for more details.
    """
    use MyAppWeb, :controller

    def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
        conn
        |> put_status(:unprocessable_entity)
        |> render(MyAppWeb.ChangesetView, "error.json", changeset: changeset)
    end

    def call(conn, {:error, _atom, %Ecto.Changeset{} = changeset, _}) do
        conn
        |> put_status(:unprocessable_entity)
        |> render(MyAppWeb.ChangesetView, "error.json", changeset: changeset)
    end

    def call(conn, {:error, :not_found}) do
        conn
        |> put_status(:not_found)
        |> render(MyAppWeb.ErrorView, "404.json")
    end

    def call(conn, {:error, :not_found, msg}) do
        conn
        |> put_status(:not_found)
        |> render(MyAppWeb.ErrorView, "404.json", msg: msg)
    end

    def call(conn, {:error, :no_credits}) do
        conn
        |> put_status(402)
        |> render(MyAppWeb.ErrorView, "no_credits.json")
    end

    def call(conn, {:error, :unprocessable_entity}) do
        conn
        |> put_status(:unprocessable_entity)
        |> render(MyAppWeb.ErrorView, "422.json")
    end

    def call(conn, {:error, :unprocessable_entity, msg}) do
        conn
        |> put_status(:unprocessable_entity)
        |> render(MyAppWeb.ErrorView, "422.json", msg: msg)
    end

    def call(conn, {:error, :unauthorized}) do
        conn
        |> put_status(:unauthorized)
        |> json(%{error_code: 1401, errors: ["Unauthorized"]})
    end

    def call(conn, {:error, :max_queries}) do
        conn
        |> put_status(:unprocessable_entity)
        |> json(%{error_code: 1422, errors: ["You have exceeded the maximum amount of queries"]})
    end
end
