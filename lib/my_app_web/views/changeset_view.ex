defmodule MyAppWeb.ChangesetView do
  use MyAppWeb, :view

  @doc """
  Traverses and translates changeset errors.

  See `Ecto.Changeset.traverse_errors/2` and
  `MyAppWeb.ErrorHelpers.translate_error/1` for more details.
  """
  def translate_errors(changeset) do
    Ecto.Changeset.traverse_errors(changeset, &translate_error/1)
  end

  def render("error.json", %{changeset: changeset}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.
    %{errors: translate_errors(changeset) |> map_to_string()}
  end

  def map_to_string(errors) do
    Enum.map(errors, fn {field, detail} ->
      "#{Gettext.gettext(MyAppWeb.Gettext, Atom.to_string(field))} #{render_detail(detail)}"
    end)
  end

  def render_detail({message, values}) do
    Enum.reduce values, message, fn {k, v}, acc ->
      String.replace(acc, "%{#{k}}", to_string(v))
    end
  end

  def render_detail(message) do
    message
  end
end
