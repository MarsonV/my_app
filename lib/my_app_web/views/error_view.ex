defmodule MyAppWeb.ErrorView do
    use MyAppWeb, :view

    def render("404.html", _assigns) do
        "Page not found"
    end

    def render("500.html", _assigns) do
        "Internal server error"
    end

    def render("404.json", assigns) do
        %{errors: [assigns[:msg] || "Resource not found"]}
    end

    def render("400.json", _assigns) do
        %{errors: ["Bad Request"], error_code: 1400}
    end

    def render("500.json", _assigns) do
        %{errors: ["Internal server error"]}
    end

    def render("401.json", _assigns) do
        %{error_code: 1401, errors: ["Unauthorized"]}
    end

    def render("422.json", assigns) do
        %{errors: [assigns[:msg] || "Bad arguments"]}
    end

    # In case no render clause matches or no
    # template is found, let's render it as 500
    def template_not_found(_template, assigns) do
        render "500.json", assigns
    end
end
