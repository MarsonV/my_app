defmodule MyApp do
    @moduledoc """
    MyApp keeps the contexts that define your domain
    and business logic.

    Contexts are also responsible for managing your data, regardless
    if it comes from the database, an external API or others.
    """

    def key_to_atom(map) do
      Enum.reduce(map, %{}, fn
        {key, value}, acc when is_atom(key) -> Map.put(acc, key, value)
        # String.to_existing_atom saves us from overloading the VM by
        # creating too many atoms. It'll always succeed because all the fields
        # in the database already exist as atoms at runtime.
        {key, value}, acc when is_binary(key) ->
          try do
            Map.put(acc, String.to_existing_atom(key), value)
          rescue
            e in ArgumentError -> Map.put(acc, String.to_atom(key), value)
          end
      end)
    end

    def key_to_string(map) do
      Enum.reduce(map, %{}, fn
        {key, value}, acc when is_binary(key) -> Map.put(acc, key, value)
        {key, value}, acc when is_atom(key) -> Map.put(acc, to_string(key), value)
      end)
    end

    def random_string(length) do
        :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
    end

    def parse_datettime(dt) when is_binary(dt), do: Timex.parse!(dt, "%Y-%m-%d", :strftime)
    def parse_datettime(%NaiveDateTime{} = dt), do: dt
end
