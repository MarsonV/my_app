use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :my_app, MyAppWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :my_app, MyApp.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "my_app",
  password: "my_app",
  database: "my_app_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :my_app, :session,
  expire_time: 604800,
  admin_token: "If6opRm0aYuRqYOgIcbea3Spnx9LkbKJRM1RTSW0EDJXaqkOg0_UPGcY1flYCOet",
  invalid_admin_token: "I123pRm0aYuRqYOgIcbea3Spnx9LkaKJRM1RTSW0EDJXaqkOg0_UPGcY1flYC322"
