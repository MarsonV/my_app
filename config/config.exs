# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :my_app,
    ecto_repos: [MyApp.Repo]
config :ecto, json_library: Poison
config :my_app, MyApp.Repo, types: MyApp.PostgresTypes

# Configures the endpoint
config :my_app, MyAppWeb.Endpoint,
    url: [host: "localhost"],
    secret_key_base: "txl43w5upqzOYqPT2LE2Vac2+uDjv8aYPH2tRMaVyvOKCulyo7TZHMfasZ/MeRh+",
    render_errors: [view: MyAppWeb.ErrorView, accepts: ~w(html json)],
    pubsub: [name: MyApp.PubSub,
                     adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
    format: "$time $metadata[$level] $message\n",
    metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.

config :canary, repo: MyApp.Repo
config :phone_number, files_path: "/priv/phone_number"
import_config "#{Mix.env}.exs"
